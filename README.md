# Usage

Have <https://gitlab.freedesktop.org/slomo/gst-plugin-custom-compositor>
built in your `GST_PLUGIN_PATH`, preferably built with --release and
https://gitlab.freedesktop.org/slomo/gst-plugin-custom-compositor/-/merge_requests/3
applied, then in a first terminal:

```
gst-launch-1.0 videotestsrc is-live=true pattern=ball ! vp8enc ! rtpvp8pay ! udpsink host=127.0.0.1 port=5000
```

and in a second terminal:

```
cargo run
```

Let it play for a bit, then interrupt the sender pipeline.

Sample output:

```
selected no sample for output running time Time(ClockTime(Some(0)))
selected no sample for output running time Time(ClockTime(Some(40000000)))
[snip]
selected no sample for output running time Time(ClockTime(Some(3480000000)))
selected no sample for output running time Time(ClockTime(Some(3520000000)))
selected sample with running time Time(ClockTime(Some(3595420946))) for output running time Time(ClockTime(Some(3560000000)))
[snip, sender is interrupted]
selected sample with running time Time(ClockTime(Some(10395415399))) for output running time Time(ClockTime(Some(10400000000)))
selected sample with running time Time(ClockTime(Some(10462081956))) for output running time Time(ClockTime(Some(10440000000)))
selected sample with running time Time(ClockTime(Some(10495415235))) for output running time Time(ClockTime(Some(10480000000)))
selected sample with running time Time(ClockTime(Some(10495415235))) for output running time Time(ClockTime(Some(10520000000)))
[snap!]
```

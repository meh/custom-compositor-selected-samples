use gst::init;
use gst::prelude::*;
use gst::Element;
use gst::ElementFactory;
use gst::MessageView;
use gst::Pipeline;
use gst::State;
use gst::CLOCK_TIME_NONE;
use gst_base;
use gst_base::prelude::*;

// udpsrc address=127.0.0.1 port=5000 ! application/x-rtp, media=video, clock-rate=90000, encoding-name="VP8" ! rtpvp8depay ! vp8dec ! capssetter caps="video/x-raw, framerate=30/1" ! videorate ! videoconvert ! queue ! custom-compositor name=comp ! autovideosink

fn main() {
    init().unwrap();

    println!("Creating pipeline");

    let pipeline_str = "custom-compositor name=comp emit-signals=true latency=200000000 sink_0::zorder=0 sink_1::zorder=1 ! autovideosink udpsrc address=127.0.0.1 port=5000 ! application/x-rtp, media=video, clock-rate=90000, encoding-name=\"VP8\" ! rtpjitterbuffer latency=200 ! rtpvp8depay wait-for-keyframe=true ! capssetter caps=\"video/x-vp8, framerate=30/1\" ! queue ! vp8dec ! videoconvert ! queue ! comp.sink_1 videotestsrc pattern=black is-live=true ! queue ! comp.sink_0";

    let pipeline =
        match gst::parse_launch_full(&pipeline_str, None, gst::ParseFlags::empty()) {
            Ok(pipeline) => pipeline,
            Err(err) => {
                eprintln!("Failed to build pipeline: {}", err);
                return;
            }
        };

    let pipeline = pipeline.dynamic_cast::<gst::Pipeline>().unwrap();
    let comp = pipeline.get_by_name("comp").unwrap();

    comp.connect("samples-selected", false, |values| {
        let comp = values[0].get::<gst::Element>().unwrap().unwrap();
        let segment = values[1].get::<gst::Segment>().unwrap().unwrap();
        let pts = values[2].get::<gst::ClockTime>().unwrap().unwrap();
        let pad = comp.get_static_pad("sink_1").unwrap();
        let agg = comp.downcast_ref::<gst_base::Aggregator>().unwrap();
        let agg_pad = pad.downcast_ref::<gst_base::AggregatorPad>().unwrap();
        let sample = agg.peek_next_sample(agg_pad);

        let ortime = segment.to_running_time (pts);

        if let Some(sample) = sample {
            let buffer = sample.get_buffer().unwrap();
            let segment = sample.get_segment().unwrap();
            let rtime = segment.to_running_time (buffer.get_pts());

            eprintln!("selected sample with running time {:?} for output running time {:?}", rtime, ortime);
        } else {
            eprintln!("selected no sample for output running time {:?}", ortime);
        }
        None
    }).unwrap();

    pipeline.set_state(State::Playing).unwrap();

    let bus = pipeline.get_bus().unwrap();
    for msg in bus.iter_timed(CLOCK_TIME_NONE) {
        match msg.view() {
            MessageView::Eos(..) => {
                println!("EOS");
                break;
            }
            MessageView::Error(err) => {
                println!(
                    "Error from {:?}: {} ({:?})",
                    err.get_src().map(|s| s.get_path_string()),
                    err.get_error(),
                    err.get_debug()
                );
                break;
            }
            _ => (),
        }
    }

    pipeline.set_state(State::Null).unwrap();
}
